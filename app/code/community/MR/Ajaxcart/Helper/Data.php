<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 23/10/14
 * Time: 2:04 PM
 */
class MR_Ajaxcart_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getMinicartHtml()
    {
        return Mage::app()->getLayout()->getBlock('minicart_content')->toHtml();
    }
}
