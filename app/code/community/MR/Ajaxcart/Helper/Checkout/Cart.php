<?php

/**
 * Created by PhpStorm.
 * User: roc
 * Date: 5/01/15
 * Time: 5:16 PM
 */
class MR_Ajaxcart_Helper_Checkout_Cart extends Mage_Checkout_Helper_Cart
{

    /**
     * @inheritdoc
     */
    public function getAddUrl($product, $additional = array())
    {
        // Generate different url depending on if AJAX is disabled.
        $isAjaxDisabled = isset($additional['is-ajax-disabled']) ?: false;
        unset($additional['is-ajax-disabled']);

        $routeParams = array(
            Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $this->_getHelperInstance('core')
                ->urlEncode($this->getCurrentUrl()),
            'product' => $product->getEntityId(),
            Mage_Core_Model_Url::FORM_KEY => $this->_getSingletonModel('core/session')->getFormKey()
        );

        if (!empty($additional)) {
            $routeParams = array_merge($routeParams, $additional);
        }

        if ($product->hasUrlDataObject()) {
            $routeParams['_store'] = $product->getUrlDataObject()->getStoreId();
            $routeParams['_store_to_url'] = true;
        }

        if ($this->_getRequest()->getRouteName() == 'checkout'
            && $this->_getRequest()->getControllerName() == 'cart'
        ) {
            $routeParams['in_cart'] = 1;
        }

        return $isAjaxDisabled ?
            $this->_getUrl('checkout/cart/add', $routeParams)
            :
            $this->_getUrl('mr_ajaxcart/cart/add', $routeParams);
    }
}
