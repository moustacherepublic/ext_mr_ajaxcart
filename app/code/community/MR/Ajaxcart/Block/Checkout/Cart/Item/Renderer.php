<?php

/**
 * Created by PhpStorm.
 * User: roc
 * Date: 8/01/15
 * Time: 5:07 PM
 */
class MR_Ajaxcart_Block_Checkout_Cart_Item_Renderer extends Mage_Checkout_Block_Cart_Item_Renderer
{
    /**
     * @inheritdoc
     */
    public function getAjaxDeleteUrl()
    {
        return $this->getUrl(
            'mr_ajaxcart/cart/delete',
            array(
                'id' => $this->getItem()->getId(),
                Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $this->helper('core/url')->getEncodedUrl(),
                '_secure' => $this->_getApp()->getStore()->isCurrentlySecure(),
            )
        );
    }

    /**
     * Returns true if user is going through checkout process now.
     *
     * @return bool
     */
    public function isOnCartPage()
    {
        $module = $this->getRequest()->getModuleName();
        $controller = $this->getRequest()->getControllerName();
        return $module === 'checkout' && $controller === 'cart';
    }
}
