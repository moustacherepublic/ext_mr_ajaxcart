<?php

class MR_Ajaxcart_Model_Observer
{
    public function onViewingProduct($observer)
    {
        $action = $observer->getAction();

        if ($action->getFullActionName() === 'catalog_product_view'
            && $action->getRequest()->getParam('mr_ajaxcart_configurable')
        ) {
            $observer->getLayout()->getUpdate()->addHandle('catalog_product_view_iframe');
        }
    }
}
