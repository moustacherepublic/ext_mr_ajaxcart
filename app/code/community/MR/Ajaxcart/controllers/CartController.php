<?php
require_once 'Mage/Checkout/controllers/CartController.php';
class MR_Ajaxcart_CartController extends Mage_Checkout_CartController
{
    /**
     * Add product to shopping cart action
     */
    public function addAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest()){
            parent::addAction();
            return;
        }
        if (!$this->_validateFormKey()) {
            $this->_goBack();
            return;
        }
        $cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();
        $result = array();
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                $result['success'] = 0;
                $result['message'] = 'Please specify a product!';
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                return;
            }

            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );

            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()){
                    $this->loadLayout();

                    $result['message'] = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));
                    $result['success'] = 1;
                    $result['count'] = $cart->getSummaryQty();
                    $result['minicart'] = Mage::helper('mr_ajaxcart')->getMinicartHtml();
                }
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
            }
        } catch (Mage_Core_Exception $e) {
            $result['success'] = 0;
            $messages = array_unique(explode("\n", $e->getMessage()));
            $result['message'] = implode('<br />', $messages);
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        } catch (Exception $e) {
            $result['success'] = 0;
            $result['message'] ='Cannot add the item to shopping cart.';
            Mage::logException($e);
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    /**
     * Action to reconfigure cart item
     */
    public function configureAction()
    {
        // Extract item and product to configure
        $id = (int) $this->getRequest()->getParam('id');
        $quoteItem = null;
        $cart = $this->_getCart();
        if ($id) {
            $quoteItem = $cart->getQuote()->getItemById($id);
        }

        if (!$quoteItem) {
            $this->_getSession()->addError($this->__('Quote item is not found.'));
            $this->_redirect('checkout/cart');
            return;
        }

        try {
            $params = new Varien_Object();
            $params->setCategoryId(false);
            $params->setConfigureMode(true);
            $params->setBuyRequest($quoteItem->getBuyRequest());

            Mage::helper('catalog/product_view')->prepareAndRender($quoteItem->getProduct()->getId(), $this, $params);
        } catch (Exception $e) {
            $this->_getSession()->addError($this->__('Cannot configure product.'));
            Mage::logException($e);
            $this->_goBack();
            return;
        }
    }

    /**
     * Delete shoping cart item action
     */
    public function deleteAction()
    {
        $result = array();
        $cart = $this->_getCart();
        $id = (int) $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $this->_getCart()->removeItem($id)->save();
                $this->loadLayout();

                $result['success'] = 1;
                $result['count'] = $cart->getSummaryQty();
                $result['minicart'] = Mage::helper('mr_ajaxcart')->getMinicartHtml();
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                $result['success'] = 0;
                $result['message'] = $this->__('Cannot remove the item.');
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                return;
            }
        }
        $result['success'] = 0;
        $result['message'] = $this->__('Please specify the product to delete.');
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }
}
