;(function ($) {
  $.mr_ajaxcart = function (options) {
    var settings = $.extend({}, $.mr_ajaxcart.defaults, options);
    var hideTimeout = false;

    var methods = {
      init: function () {
        // Toggle mini cart dropdown
        $(settings.miniCartNavContainerSelector).hover(methods.showMiniCart, methods.hideMiniCart);

        // Logic for 'Add to cart' button, on product form submit
        $('body').on('submit', '#product_addtocart_form', function(e){
          e.preventDefault();
          fireEvent($(this).find(settings.addToCartSelector).get(0), 'click');
        }).on('click', settings.addToCartSelector, methods.addToCart);

        $(settings.miniCartSelector).on('click', '.js-btn-remove', methods.delCartItem);

        $(window).on('message', methods.handleMessage);
      },

      handleMessage: function (e) {

        // Check the origin for security reason
        if (e.originalEvent.origin !== location.origin) {
          return;

        }

        $.fancybox.close();

        var response = JSON.parse(e.originalEvent.data);
        methods.processResponse.call('', response);
      },

      delCartItem: function (e) {

        e.preventDefault();
        var delButton = $(this);

        if (confirm(delButton.data('confirm'))) {

          if ($(this).is('[disable-ajax]')) {
            window.location = delButton.attr('href');
            return;
          }

          $.ajax(delButton.attr('href'), {
            type    : 'GET',
            dataType: 'JSON',
            context : this
          }).done(methods.processResponse);

        }
      },

      addToCart: function () {
        // var this = the button.
        var url, type, data;

        if (window.productAddToCartForm && $.contains(window.productAddToCartForm.form, this)) {
          // Add to cart via form

          if (window.productAddToCartForm.validator && !window.productAddToCartForm.validator.validate()) {
            return;
          }

          url = window.productAddToCartForm.form.action;
          type = 'POST';
          //fix for enterprise FPC
          if (Mage.Cookies.get('CACHED_FRONT_FORM_KEY') != null){
            $(window.productAddToCartForm.form).find('input[name="form_key"]').attr('value', Mage.Cookies.get('CACHED_FRONT_FORM_KEY'));
          }
          data = $(window.productAddToCartForm.form).serialize();
        } else {
          // Add to cart via URL

          url = $(this).data('add-to-cart-url');
          type = 'GET';
          data = null;

          //Add product with options to cart
          if ($(this).is('[configurable]')) {
            $.fancybox({
              type     : 'iframe',
              href     : url + '?mr_ajaxcart_configurable=1',
              maxWidth : 320,
              minHeight : 400,
              fitToView: true
            });
            return;
          }

          if ($(this).is('[disable-ajax]')) {
            window.location = url;
            return;
          }
        }

        $(this).prop('disabled', true).button('loading');

        $.ajax(url, {
          type    : type,
          data    : data,
          dataType: 'JSON',
          context : this
        }).done(methods.processResponse);
      },

      processResponse: function (response) {
        // var this = the button.

        $(this).button('reset').prop('disabled', false);

        if (response.success) {

          if (typeof response.count === 'number') {
            $(settings.miniCartCounterSelector).text(response.count);
          }

          if (response.minicart) {
            $(settings.miniCartSelector).html(response.minicart);
          }

          if (typeof settings.afterLoad === 'function') {
            settings.afterLoad();
          }

          // Show and hide the "Added!" overlay.
          var overlay = $(this).closest('.js-overlay-container').find('.js-overlay').addClass('is-visible');
          setTimeout(function () {
            overlay.removeClass('is-visible');
          }, settings.transitionDuration);


          methods.showMiniCart();
          hideTimeout = setTimeout(function () {
            methods.hideMiniCart();
          }, settings.transitionDuration);

          if (window.self !== window.top) {
            // We are in a iframe, so inform the parent window with the response.
            window.parent.postMessage(JSON.stringify(response), location.origin);
          }

        } else if (response.message) {

          var msgContainer = $('#messages_product_view');

          if (msgContainer.length) {
            $('#messages_product_view').html(
              '<ul class="messages"><li class="error-msg"><ul><li><span>'
              + response.message
              + '</span></li></ul></li></ul>'
            );
          } else {
            alert(response.message);
          }

        }
      },

      showMiniCart: function () {
        if (hideTimeout) {
          clearTimeout(hideTimeout);
        }
        $(settings.miniCartNavContainerSelector).find(settings.miniCartSelector).addClass('is-visible');
      },

      hideMiniCart: function () {
        $(settings.miniCartNavContainerSelector).find(settings.miniCartSelector).removeClass('is-visible');
      }
    };

    methods.init();
  };

  $.mr_ajaxcart.defaults = {
    addToCartSelector           : '.btn-cart',
    miniCartNavContainerSelector: '.js-mini-cart-container',
    miniCartSelector            : '.js-minicart',
    miniCartCounterSelector     : '.js-cart-count',

    transitionDuration: 2000,
    afterLoad         : null
  };
})(jQuery);

jQuery(function() {
  // Initialize AJAX cart
  jQuery.mr_ajaxcart();
});
